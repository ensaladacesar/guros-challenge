# Guros fullstack dev challenge

This project was built with NestJS, Mongo and Express in integration with Docker and Gitlab's CI/CD workflow.

## Installation

Just need to install dependencies

```bash
npm i
```

## Usage
You can find API documentation at /api.

### Examples:

```curl
# Returns mutations statistics
curl --location --request GET 'http://guros-challenge.herokuapp.com/dnas/stats'

# Tests DNA chain
curl --location --request POST 'http://guros-challenge.herokuapp.com/dnas/mutation' \
--header 'Content-Type: application/json' \
--data-raw '{
    "dna": [
        "ATACGG",
        "ACGTGC",
        "ATAGGT",
        "CTATGA",
        "CTTCTA",
        "TAACAG"
    ]
}'

