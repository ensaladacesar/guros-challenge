import { UsersModule } from './users/users.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DnasModule } from './dnas/dnas.module';
import { ConfigModule } from '@nestjs/config';
import { ConfigurationService } from './configuration/configuration/configuration.service';
import { join } from 'path';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: `mongodb://${process.env.MONGODBUSER}:${process.env.MONGODBPASSWORD}@${process.env.MONGODBHOST}:${process.env.MONGODBPORT}/${process.env.MONGODBDATABASE}`,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      authSource: "admin",
      entities: [join(__dirname, '**', '*.entity.{ts,js}')],
      synchronize: true,
      logging: true
  }),
    DnasModule,
    UsersModule
  ],
  controllers: [AppController],
  providers: [AppService, ConfigurationService],
})
export class AppModule {
  static port: number;
  constructor(private readonly configurationService: ConfigurationService) {
    AppModule.port = this.configurationService.port as number;
  }
}
