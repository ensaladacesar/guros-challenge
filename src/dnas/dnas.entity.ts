import { Column, Entity, PrimaryColumn, ObjectIdColumn, ObjectID } from "typeorm";

@Entity('dnas')
export class DnasEntity {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    dna: string[];

    @Column()
    mutation: boolean;

    constructor(dna: string[], mutation: boolean) {
        this.dna = dna;
        this.mutation = mutation;
    }

}