import { Injectable } from '@nestjs/common';
import { DnasDTO } from './dnas.dto';
import { DnasEntity } from './dnas.entity';
import { DnasMapper } from './dnas.mapper';
import { DnasRepository } from './dnas.repository';

@Injectable()
export class DnasService {
    constructor(private dnasRepository: DnasRepository,
        private mapper: DnasMapper) { }

    async getAllDnas(): Promise<DnasDTO[]> {
        const dnas: DnasEntity[] = await this.dnasRepository.getAllDnas()
        return dnas.map(dna => this.mapper.entityToDto(dna));
    }

    async newDnas(dnaDTO: DnasDTO): Promise<DnasDTO> {
        const newDna: DnasEntity = await this.dnasRepository.newDnas(dnaDTO);
        return this.mapper.entityToDto(newDna);
    }

    async findDnas(dnaDTO: DnasDTO): Promise<DnasDTO> {
        const newDna: DnasEntity = await this.dnasRepository.getDnasBySecuence(dnaDTO.dna);
        return this.mapper.entityToDto(newDna);
    }

}
