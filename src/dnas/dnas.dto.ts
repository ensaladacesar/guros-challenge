import { ApiProperty } from "@nestjs/swagger";

export class DnasDTO {

    @ApiProperty()
    dna: string[];

    @ApiProperty()
    mutation: boolean

    constructor(dna: string[], mutation: boolean) {
        this.dna = dna;
        this.mutation = mutation;
    }
}