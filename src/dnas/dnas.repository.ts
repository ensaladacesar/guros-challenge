import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { DnasDTO } from './dnas.dto';
import { DnasEntity } from './dnas.entity';
import { DnasMapper } from './dnas.mapper';

@Injectable()
export class DnasRepository {

    constructor(
        @InjectRepository(DnasEntity)
        private dnasRepository: Repository<DnasEntity>,
        private mapper: DnasMapper) { }

    getAllDnas(): Promise<DnasEntity[]> {
        return this.dnasRepository.find();
    }

    getDnasBySecuence(dna: string[]): Promise<DnasEntity> {
        return this.dnasRepository.findOne({ dna });
    }

    newDnas(dnasDTO: DnasDTO): Promise<DnasEntity> {
        const newDnas = this.mapper.dtoToEntity(dnasDTO);
        return this.dnasRepository.save(newDnas);
    }

}