import { DnasDTO } from './dnas.dto';
import { Injectable } from "@nestjs/common";
import { DnasEntity } from './dnas.entity';

@Injectable()
export class DnasMapper {

    dtoToEntity(dnaDTO: DnasDTO): DnasEntity {
        return new DnasEntity(dnaDTO.dna, dnaDTO.mutation);
    }

    entityToDto(dnaEntity: DnasEntity): DnasDTO {
        return new DnasDTO(dnaEntity.dna, dnaEntity.mutation);
    }

}