import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DnasEntity } from './dnas.entity';
import { DnasController } from './dnas.controller';
import { DnasService } from './dnas.service';
import { DnasMapper } from './dnas.mapper';
import { DnasRepository } from './dnas.repository';
@Module({
  imports: [TypeOrmModule.forFeature([DnasEntity]),
  JwtModule.register({
    secret: `${process.env.JWT_SECRET}`,
    signOptions: { expiresIn: '20h' },
  }),
    PassportModule],
  controllers: [DnasController],
  providers: [DnasService, DnasRepository, DnasMapper]
})
export class DnasModule { }
