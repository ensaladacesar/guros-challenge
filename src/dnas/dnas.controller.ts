import { Controller, Get, Post, Res, Body, HttpStatus, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Response } from 'express';
import { DnasDTO } from './dnas.dto';
import { DnasService } from './dnas.service';

@Controller('dnas')
export class DnasController {

    constructor(private dnasService: DnasService) { }

    dnas: DnasDTO[] = [];
    @Get('stats')
    // @ApiBearerAuth()
    // @UseGuards(AuthGuard('jwt'))
    async getAllDnas() {
        const dnas = await this.dnasService.getAllDnas();

        const count_mutations = dnas.filter(dna => dna.mutation).length
        const count_no_mutation = dnas.length - count_mutations;
        const ratio = count_mutations / count_no_mutation

        return {
            count_mutations,
            count_no_mutation,
            ratio,
        }
    }

    @Post('mutation')
    // @ApiBearerAuth()
    // @UseGuards(AuthGuard('jwt'))
    async newDna(@Body() mutations: DnasDTO, @Res() res: Response) {

        try {
            const mut = await this.dnasService.findDnas(mutations);
            return mut.mutation ? res.status(HttpStatus.OK).send() : res.status(HttpStatus.FORBIDDEN).send()
        } catch (error) {
            mutations.mutation = this.hasMutation(mutations.dna)
            await this.dnasService.newDnas(mutations);
            return mutations.mutation ? res.status(HttpStatus.OK).send() : res.status(HttpStatus.FORBIDDEN).send()
        }
    }

    hasMutation(dna: string[]): boolean {
        let dnaArray = [[]];
        dnaArray = this.destructureDnaArray(dna);
        let sequences = 0;
        for (let y = 0; y < dnaArray[0].length; y++) {
            for (let x = 0; x < dnaArray.length; x++) {
                let character = dnaArray[y][x];
                if (this.testVertical(y, x, character, dnaArray))
                    sequences++;
                if (this.testHorizontal(y, x, character, dnaArray))
                    sequences++;
                if (this.testDiagonal(y, x, character, dnaArray))
                    sequences++;
                if (sequences > 1)
                    break;
            }
            if (sequences > 1)
                break;
        }
        if (sequences > 1) {
            return true;
        }
        else
            return false;
    }

    destructureDnaArray(dnaArray: string[]): Array<string[]> {
        let destructuredDNAArray = [[]];
        for (let i = 0; i < dnaArray.length; i++) {
            destructuredDNAArray[i] = new Array(dnaArray.length);
        }
        dnaArray.forEach((dna, X) => {
            [...dna].forEach((singleChar, Y) => {
                destructuredDNAArray[X][Y] = singleChar;
            })
        });
        return destructuredDNAArray;
    }

    testVertical(Y: number, X: number, character: string, dnaArray: Array<string[]>) {
        if (3 - (Y + 1) < 0) {
            return false;
        }
        let result = true;
        for (let x = 1; x < 4; x++) {
            if (dnaArray[Y + x][X] !== character) {
                result = false;
                break;
            }
        }
        return result;
    }

    testHorizontal(Y: number, X: number, character: string, dnaArray: Array<string[]>) {
        if (3 - (X + 1) < 0) {
            return false;
        }
        let result = true;
        for (let x = 1; x < 4; x++) {
            if (dnaArray[Y][X + x] !== character) {
                result = false;
                break;
            }
        }
        return result;
    }

    testDiagonal(Y: number, X: number, character: string, dnaArray: Array<string[]>) {
        if ((3 - (X + 1) < 0) || (3 - (Y + 1) < 0)) {
            return false;
        }
        let result = true;
        for (let x = 1; x < 4; x++) {
            if (dnaArray[Y + x][X + x] !== character) {
                result = false;
                break;
            }
        }
        return result;
    }
}
