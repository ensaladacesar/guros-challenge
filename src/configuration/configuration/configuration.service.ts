import { Injectable } from '@nestjs/common';

@Injectable()
export class ConfigurationService {
    port;

    constructor(){
        this.port = process.env.PORT || 3000
    }
}
