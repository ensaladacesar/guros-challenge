import { UsersDTO } from './users.dto';
import * as bcrypt from 'bcrypt';
import { ObjectID } from 'typeorm';
import { UsersRepository } from './users.repository';
import { Injectable } from '@nestjs/common';
import { UsersEntity } from './users.entity';
import { JwtService } from '@nestjs/jwt';

export interface JWTPayload {
    userId: ObjectID;
}

@Injectable()
export class UsersService {

    constructor(private usersRepository: UsersRepository,
        private jwtService: JwtService) { }

    async getUserByName(name: string): Promise<UsersEntity> {
        return await this.usersRepository.getUserByName(name);
    }

    async newUser(user: UsersDTO): Promise<UsersEntity> {
        return await this.usersRepository.newUsers(user);
    }

    async validateUser(username: string, pass: string): Promise<boolean> {
        const user = await this.usersRepository.getUserByName(username);
        return await this.validatePassword(pass, user.password);
    }

    async generateAccessToken(name: string) {
        const user = await this.usersRepository.getUserByName(name);
        const payload: JWTPayload = { userId: user._id };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    async validatePassword(password: string, userPassword: string): Promise<boolean> {
        return bcrypt.compareSync(password, userPassword);
    }
}
