import { ApiProperty } from "@nestjs/swagger";
import { ObjectID } from "typeorm";

export class UsersDTO {

    @ApiProperty()
    readonly name: string;

    @ApiProperty()
    password: string;


    constructor(name: string, password: string) {
        this.name = name;
        this.password = password;
    }
}

export class LoginDTO {
    @ApiProperty()
    name: string;

    @ApiProperty()
    password: string;
  }