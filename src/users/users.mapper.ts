import { UsersEntity } from './users.entity';
import { UsersDTO } from './users.dto';
import { Injectable } from "@nestjs/common";

@Injectable()
export class UsersMapper {

    dtoToEntity(usersDTO: UsersDTO): UsersEntity {
        return new UsersEntity(usersDTO.name, usersDTO.password);
    }

    entityToDto(usersEntity: UsersEntity): UsersDTO {
        return new UsersDTO(usersEntity.name, usersEntity.password);
    }

}