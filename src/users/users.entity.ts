import * as bcrypt from 'bcrypt';
import {
    BeforeInsert, Column, Entity, ObjectIdColumn,
    ObjectID
} from 'typeorm';

@Entity('users')
export class UsersEntity {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column({
        unique: true,
    })
    name: string;

    @Column({ type: 'varchar', length: 70, nullable: true })
    password: string;

    @BeforeInsert()
    async hashPassword() {
        const salt = await bcrypt.genSalt();
        this.password = await bcrypt.hash(this.password, salt);
    }

    constructor(name: string, pass: string, userId?: ObjectID) {
        this._id = userId ? userId : null

        this.name = name;
        this.password = pass;
    }
}