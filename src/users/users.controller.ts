import { UsersService } from './users.service';
import { LoginDTO } from './users.dto';
import { UsersDTO } from './users.dto';
import {
    Controller, Post, Get, Body, UnauthorizedException, UseGuards, Param, Put,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) { }
    @Post('login')
    async login(@Body() loginDTO: LoginDTO): Promise<{ access_token: string }> {
        const { name, password } = loginDTO;
        const valid = await this.usersService.validateUser(name, password);
        if (!valid) {
            throw new UnauthorizedException();
        }
        return await this.usersService.generateAccessToken(name);
    }

    @Post()
    async newUser(@Body() user: UsersDTO) {
        return await this.usersService.newUser(user);
    }

}
