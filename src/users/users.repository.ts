import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersDTO } from './users.dto';
import { UsersEntity } from './users.entity';
import { UsersMapper } from './users.mapper';

@Injectable()
export class UsersRepository {

    constructor(
        @InjectRepository(UsersEntity)
        private usersRepository: Repository<UsersEntity>,
        private mapper: UsersMapper) { }

    getAllUsers(): Promise<UsersEntity[]> {
        return this.usersRepository.find();
    }

    getUserByName(name: string): Promise<UsersEntity> {
        return this.usersRepository.findOne({ name });
    }

    newUsers(usersDTO: UsersDTO): Promise<UsersEntity> {
        const newUsers = this.mapper.dtoToEntity(usersDTO);
        return this.usersRepository.save(newUsers);
    }

}