import { join } from 'path';
import { ConnectionOptions } from 'typeorm';

const config: ConnectionOptions = {
    type: 'mongodb',
    url: `mongodb://${process.env.MONGODBUSER}:${process.env.MONGODBPASSWORD}@${process.env.MONGODBHOST}:${process.env.MONGODBPORT}/${process.env.MONGODBDATABASE}`,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    authSource: "admin",
    entities: [join(__dirname, '**', '*.entity.{ts,js}')],
    synchronize: true,
    logging: true
}

export = config;