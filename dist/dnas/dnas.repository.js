"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DnasRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const dnas_entity_1 = require("./dnas.entity");
const dnas_mapper_1 = require("./dnas.mapper");
let DnasRepository = class DnasRepository {
    constructor(dnasRepository, mapper) {
        this.dnasRepository = dnasRepository;
        this.mapper = mapper;
    }
    getAllDnas() {
        return this.dnasRepository.find();
    }
    getDnasBySecuence(dna) {
        return this.dnasRepository.findOne({ dna });
    }
    newDnas(dnasDTO) {
        const newDnas = this.mapper.dtoToEntity(dnasDTO);
        return this.dnasRepository.save(newDnas);
    }
};
DnasRepository = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(dnas_entity_1.DnasEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        dnas_mapper_1.DnasMapper])
], DnasRepository);
exports.DnasRepository = DnasRepository;
//# sourceMappingURL=dnas.repository.js.map