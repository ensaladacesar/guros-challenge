"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DnasService = void 0;
const common_1 = require("@nestjs/common");
const dnas_mapper_1 = require("./dnas.mapper");
const dnas_repository_1 = require("./dnas.repository");
let DnasService = class DnasService {
    constructor(dnasRepository, mapper) {
        this.dnasRepository = dnasRepository;
        this.mapper = mapper;
    }
    async getAllDnas() {
        const dnas = await this.dnasRepository.getAllDnas();
        return dnas.map(dna => this.mapper.entityToDto(dna));
    }
    async newDnas(dnaDTO) {
        const newDna = await this.dnasRepository.newDnas(dnaDTO);
        return this.mapper.entityToDto(newDna);
    }
    async findDnas(dnaDTO) {
        const newDna = await this.dnasRepository.getDnasBySecuence(dnaDTO.dna);
        return this.mapper.entityToDto(newDna);
    }
};
DnasService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [dnas_repository_1.DnasRepository,
        dnas_mapper_1.DnasMapper])
], DnasService);
exports.DnasService = DnasService;
//# sourceMappingURL=dnas.service.js.map