import { Response } from 'express';
import { DnasDTO } from './dnas.dto';
import { DnasService } from './dnas.service';
export declare class DnasController {
    private dnasService;
    constructor(dnasService: DnasService);
    dnas: DnasDTO[];
    getAllDnas(): Promise<{
        count_mutations: number;
        count_no_mutation: number;
        ratio: number;
    }>;
    newDna(mutations: DnasDTO, res: Response): Promise<Response<any, Record<string, any>>>;
    hasMutation(dna: string[]): boolean;
    destructureDnaArray(dnaArray: string[]): Array<string[]>;
    testVertical(Y: number, X: number, character: string, dnaArray: Array<string[]>): boolean;
    testHorizontal(Y: number, X: number, character: string, dnaArray: Array<string[]>): boolean;
    testDiagonal(Y: number, X: number, character: string, dnaArray: Array<string[]>): boolean;
}
