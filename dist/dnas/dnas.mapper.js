"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DnasMapper = void 0;
const dnas_dto_1 = require("./dnas.dto");
const common_1 = require("@nestjs/common");
const dnas_entity_1 = require("./dnas.entity");
let DnasMapper = class DnasMapper {
    dtoToEntity(dnaDTO) {
        return new dnas_entity_1.DnasEntity(dnaDTO.dna, dnaDTO.mutation);
    }
    entityToDto(dnaEntity) {
        return new dnas_dto_1.DnasDTO(dnaEntity.dna, dnaEntity.mutation);
    }
};
DnasMapper = __decorate([
    (0, common_1.Injectable)()
], DnasMapper);
exports.DnasMapper = DnasMapper;
//# sourceMappingURL=dnas.mapper.js.map