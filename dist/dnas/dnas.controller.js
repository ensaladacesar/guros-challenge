"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DnasController = void 0;
const common_1 = require("@nestjs/common");
const dnas_dto_1 = require("./dnas.dto");
const dnas_service_1 = require("./dnas.service");
let DnasController = class DnasController {
    constructor(dnasService) {
        this.dnasService = dnasService;
        this.dnas = [];
    }
    async getAllDnas() {
        const dnas = await this.dnasService.getAllDnas();
        const count_mutations = dnas.filter(dna => dna.mutation).length;
        const count_no_mutation = dnas.length - count_mutations;
        const ratio = count_mutations / count_no_mutation;
        return {
            count_mutations,
            count_no_mutation,
            ratio,
        };
    }
    async newDna(mutations, res) {
        try {
            const mut = await this.dnasService.findDnas(mutations);
            return mut.mutation ? res.status(common_1.HttpStatus.OK).send() : res.status(common_1.HttpStatus.FORBIDDEN).send();
        }
        catch (error) {
            mutations.mutation = this.hasMutation(mutations.dna);
            await this.dnasService.newDnas(mutations);
            return mutations.mutation ? res.status(common_1.HttpStatus.OK).send() : res.status(common_1.HttpStatus.FORBIDDEN).send();
        }
    }
    hasMutation(dna) {
        let dnaArray = [[]];
        dnaArray = this.destructureDnaArray(dna);
        let sequences = 0;
        for (let y = 0; y < dnaArray[0].length; y++) {
            for (let x = 0; x < dnaArray.length; x++) {
                let character = dnaArray[y][x];
                if (this.testVertical(y, x, character, dnaArray))
                    sequences++;
                if (this.testHorizontal(y, x, character, dnaArray))
                    sequences++;
                if (this.testDiagonal(y, x, character, dnaArray))
                    sequences++;
                if (sequences > 1)
                    break;
            }
            if (sequences > 1)
                break;
        }
        if (sequences > 1) {
            return true;
        }
        else
            return false;
    }
    destructureDnaArray(dnaArray) {
        let destructuredDNAArray = [[]];
        for (let i = 0; i < dnaArray.length; i++) {
            destructuredDNAArray[i] = new Array(dnaArray.length);
        }
        dnaArray.forEach((dna, X) => {
            [...dna].forEach((singleChar, Y) => {
                destructuredDNAArray[X][Y] = singleChar;
            });
        });
        return destructuredDNAArray;
    }
    testVertical(Y, X, character, dnaArray) {
        if (3 - (Y + 1) < 0) {
            return false;
        }
        let result = true;
        for (let x = 1; x < 4; x++) {
            if (dnaArray[Y + x][X] !== character) {
                result = false;
                break;
            }
        }
        return result;
    }
    testHorizontal(Y, X, character, dnaArray) {
        if (3 - (X + 1) < 0) {
            return false;
        }
        let result = true;
        for (let x = 1; x < 4; x++) {
            if (dnaArray[Y][X + x] !== character) {
                result = false;
                break;
            }
        }
        return result;
    }
    testDiagonal(Y, X, character, dnaArray) {
        if ((3 - (X + 1) < 0) || (3 - (Y + 1) < 0)) {
            return false;
        }
        let result = true;
        for (let x = 1; x < 4; x++) {
            if (dnaArray[Y + x][X + x] !== character) {
                result = false;
                break;
            }
        }
        return result;
    }
};
__decorate([
    (0, common_1.Get)('stats'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DnasController.prototype, "getAllDnas", null);
__decorate([
    (0, common_1.Post)('mutation'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dnas_dto_1.DnasDTO, Object]),
    __metadata("design:returntype", Promise)
], DnasController.prototype, "newDna", null);
DnasController = __decorate([
    (0, common_1.Controller)('dnas'),
    __metadata("design:paramtypes", [dnas_service_1.DnasService])
], DnasController);
exports.DnasController = DnasController;
//# sourceMappingURL=dnas.controller.js.map