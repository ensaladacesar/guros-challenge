import { Repository } from 'typeorm';
import { DnasDTO } from './dnas.dto';
import { DnasEntity } from './dnas.entity';
import { DnasMapper } from './dnas.mapper';
export declare class DnasRepository {
    private dnasRepository;
    private mapper;
    constructor(dnasRepository: Repository<DnasEntity>, mapper: DnasMapper);
    getAllDnas(): Promise<DnasEntity[]>;
    getDnasBySecuence(dna: string[]): Promise<DnasEntity>;
    newDnas(dnasDTO: DnasDTO): Promise<DnasEntity>;
}
