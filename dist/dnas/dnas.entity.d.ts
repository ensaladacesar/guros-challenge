import { ObjectID } from "typeorm";
export declare class DnasEntity {
    id: ObjectID;
    dna: string[];
    mutation: boolean;
    constructor(dna: string[], mutation: boolean);
}
