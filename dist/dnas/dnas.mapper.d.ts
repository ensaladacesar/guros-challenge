import { DnasDTO } from './dnas.dto';
import { DnasEntity } from './dnas.entity';
export declare class DnasMapper {
    dtoToEntity(dnaDTO: DnasDTO): DnasEntity;
    entityToDto(dnaEntity: DnasEntity): DnasDTO;
}
