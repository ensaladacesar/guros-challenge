export declare class DnasDTO {
    dna: string[];
    mutation: boolean;
    constructor(dna: string[], mutation: boolean);
}
