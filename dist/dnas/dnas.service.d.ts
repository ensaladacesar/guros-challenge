import { DnasDTO } from './dnas.dto';
import { DnasMapper } from './dnas.mapper';
import { DnasRepository } from './dnas.repository';
export declare class DnasService {
    private dnasRepository;
    private mapper;
    constructor(dnasRepository: DnasRepository, mapper: DnasMapper);
    getAllDnas(): Promise<DnasDTO[]>;
    newDnas(dnaDTO: DnasDTO): Promise<DnasDTO>;
    findDnas(dnaDTO: DnasDTO): Promise<DnasDTO>;
}
