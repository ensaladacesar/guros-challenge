"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var AppModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const users_module_1 = require("./users/users.module");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const dnas_module_1 = require("./dnas/dnas.module");
const config_1 = require("@nestjs/config");
const configuration_service_1 = require("./configuration/configuration/configuration.service");
const path_1 = require("path");
let AppModule = AppModule_1 = class AppModule {
    constructor(configurationService) {
        this.configurationService = configurationService;
        AppModule_1.port = this.configurationService.port;
    }
};
AppModule = AppModule_1 = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot(),
            typeorm_1.TypeOrmModule.forRoot({
                type: 'mongodb',
                url: `mongodb://${process.env.MONGODBUSER}:${process.env.MONGODBPASSWORD}@${process.env.MONGODBHOST}:${process.env.MONGODBPORT}/${process.env.MONGODBDATABASE}`,
                useNewUrlParser: true,
                useUnifiedTopology: true,
                authSource: "admin",
                entities: [(0, path_1.join)(__dirname, '**', '*.entity.{ts,js}')],
                synchronize: true,
                logging: true
            }),
            dnas_module_1.DnasModule,
            users_module_1.UsersModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService, configuration_service_1.ConfigurationService],
    }),
    __metadata("design:paramtypes", [configuration_service_1.ConfigurationService])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map