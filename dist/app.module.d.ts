import { ConfigurationService } from './configuration/configuration/configuration.service';
export declare class AppModule {
    private readonly configurationService;
    static port: number;
    constructor(configurationService: ConfigurationService);
}
